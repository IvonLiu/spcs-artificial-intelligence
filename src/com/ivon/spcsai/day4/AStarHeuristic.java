package com.ivon.spcsai.day4;

public interface AStarHeuristic {
	public int getCost(Board state, Board goalState);
}
